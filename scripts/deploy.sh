#!/bin/bash

set -e
AWS_REGION="us-east-1"

echo "Collecting data..."

# Get a private subnet ID based on tag
PRIVATE_SUBNET_ID=$(aws ec2 describe-subnets --region $AWS_REGION --filters "Name=tag:Name,Values=prod-private-*" --query "Subnets[0].SubnetId" --output text)

# Get the VPC ID from the subnet ID
VPC_ID=$(aws ec2 describe-subnets --region $AWS_REGION --subnet-ids $PRIVATE_SUBNET_ID --query "Subnets[0].VpcId" --output text)

# Get the security group ID
ECS_GROUP_ID=$(aws ec2 describe-security-groups --region $AWS_REGION --filters Name=vpc-id,Values=$VPC_ID Name=group-name,Values=prod-ecs-backend --query "SecurityGroups[*][GroupId]" --output text)

# Get the private subnet IDs within the same VPC
PRIVATE_SUBNET_IDS=$(aws ec2 describe-subnets --region $AWS_REGION --filters "Name=vpc-id,Values=$VPC_ID" "Name=tag:Name,Values=prod-private-*" --query "Subnets[*].SubnetId" --output text | tr '\n' ' ' | xargs -n1 echo | sed 's/^/"/;s/$/"/' | paste -sd, -)

echo "Running migration task..."
NETWORK_CONFIGURATION="{\"awsvpcConfiguration\": {\"subnets\": [$PRIVATE_SUBNET_IDS], \"securityGroups\": [\"$ECS_GROUP_ID\"], \"assignPublicIp\": \"DISABLED\"}}"
MIGRATION_TASK_ARN=$(aws ecs run-task --region $AWS_REGION --cluster prod --task-definition backend-migration --count 1 --launch-type FARGATE --network-configuration "$NETWORK_CONFIGURATION" --query 'tasks[*][taskArn]' --output text)
echo "Task $MIGRATION_TASK_ARN running..."
aws ecs wait tasks-stopped --region $AWS_REGION --cluster prod --tasks "$MIGRATION_TASK_ARN"

echo "Updating web..."
aws ecs update-service --region $AWS_REGION --cluster prod --service prod-backend-web --force-new-deployment --query "service.serviceName" --output json

echo "Done!"
